import middy from "@middy/core";
import chromium from "chrome-aws-lambda";
import { APIGatewayEvent } from "aws-lambda";
import doNotWaitForEmptyEventLoop from "@middy/do-not-wait-for-empty-event-loop";
import * as fileSystem from 'fs';
import * as pathRef from 'path';
import * as templateExp from 'handlebars';
import 'chart.js';

const data = {
  title: "A new Brazilian School",
  date: "05/12/2018",
  name: "Rodolfo Luis Marcos",
  age: 28,
  birthdate: "12/07/1990",
  course: "Computer Science",
  obs: "Graduated in 2014 by Federal University of Lavras, work with Full-Stack development and E-commerce."
}

const handler = async (event: APIGatewayEvent) => {
  const executablePath = process.env.IS_OFFLINE
    ? null
    : await chromium.executablePath;
  const browser = await chromium.puppeteer.launch({
    headless: true,
    args: chromium.args,
    defaultViewport: chromium.defaultViewport,
    executablePath
  });

  var options = {
    width: '1230px',
    headerTemplate: "<p></p>",
    footerTemplate: "<p></p>",
    displayHeaderFooter: false,
    margin: {
      top: "10px",
      bottom: "30px"
    },
    printBackground: true,
  }

  const page = await browser.newPage();

  let fileName = createPDF(data);

  await page.setJavaScriptEnabled(true);

  await page.goto(`file://${fileName}`, {
    waitUntil: "networkidle2"
  });

  setConsoleLogging(page);

  await injectScripts(page);

  const divEle = await page.$('div.test');

  await page.evaluate(async(divEle: any) => {
    (divEle as HTMLElement).innerHTML = 'Testing whether the div is rendered';
  }, divEle);

  const canvasEle = await page.$('#bar-chart');

  await page.evaluate(async(canvasEle: any) => {
    const output = (canvasEle as HTMLCanvasElement).getContext('2d');
    // @ts-ignore
    configureBarChart(output);
  }, canvasEle);

  const lineEle = await page.$('#line-chart');

  await page.evaluate(async(lineEle: any) => {
    const output = (lineEle as HTMLCanvasElement).getContext('2d');
    // @ts-ignore
    configureLineChart(output);
  }, lineEle);

  const htmlOutput = await page.evaluate(() => {
    const html = new XMLSerializer().serializeToString(document.doctype) + document.documentElement.outerHTML;
    return Promise.resolve(html);
  });
  console.log("Full Html output : ", htmlOutput)

  const pdfStream = await page.pdf(options);

  await browser.close();

  return {
    statusCode: 200,
    isBase64Encoded: true,
    headers: {
      "Content-type": "application/pdf"
    },
    body: pdfStream.toString("base64")
  };
};

function createPDF(data: any): string {

  var templateHtml = fileSystem.readFileSync(pathRef.resolve(__dirname, 'template.html'), 'utf8');
  var template = templateExp.compile(templateHtml);
  var html = template(data);

  fileSystem.writeFileSync('/tmp/testing.html', html);

  const fileName = '/tmp/testing.html';

  return fileName;
}

function setConsoleLogging(page: any) {
  page.on('console', (msg: any) => {
    for (let i = 0; i < msg.args().length; ++i)
      console.log(`${i}: ${msg.args()[i]}`);
  });
}

async function injectScripts(page: any) {
  const chartOptions01 = {
    path: pathRef.resolve(process.cwd(), 'node_modules/chart.js/dist/Chart.bundle.js'),
    type: 'text/javascript'
  }
  await page.addScriptTag(chartOptions01);

  const chartOptions02 = {
    path: pathRef.resolve(process.cwd(), 'node_modules/chart.js/dist/Chart.bundle.min.js'),
    type: 'text/javascript'
  }
  await page.addScriptTag(chartOptions02);

  const scriptOptions = {
    path: pathRef.resolve(process.cwd(), 'functions/chart-puppeteer.js'),
    type: 'text/javascript'
  }
  await page.addScriptTag(scriptOptions);
}

export const generate = middy(handler).use(doNotWaitForEmptyEventLoop());

